const models = require('../models')

/*
  EN CASO DE NO SABER USAR SEQUELIZE ORM, UTILIZAR RAW QUERIES (SQL NATIVO)
*/

class TaskService {
    /**
     * 1. (usando sequelize)
     * Determinar si la construcción del método sería un problema, en caso de serlo, refactorizar el método.
     * @param taskIds
     * @returns Promise<Task[]>
     */
    getBatch(taskIds) {
        return Promise.all(
            taskIds.map((taskId) => {
                return models.Task.findByPk(taskId)
            })
        )
    }

    /**
     * 1. (usando raw queries)
     * Determinar si la construcción del método sería un problema, en caso de serlo, arreglarlo e indicar cual sería.
     * @param taskIds
     * @returns Promise<Task[]>
     */
    // getBatch(taskIds) {
    //     return Promise.all(
    //         taskIds.map(async (taskId) => {
    //             const result = await models.Task.sequelize.query(
    //                 `SELECT * FROM task WHERE id = ${taskId}`
    //             )
    //             return result[0]
    //         })
    //     )
    // }

    /**
     * 2. (usando sequelize o con raw query)
     * Refactorizar el método usando async/await y verificar funcionamiento.
     * @param taskIds
     * @returns Promise<Task[]>
     */
    update(task) {
        return new Promise((resolve, reject) => {
            models.Task.findByPk(task.id)
                .then((t) => {
                    t.update(task)
                        .then(resolve)
                        .catch(reject)
                })
                .catch(reject)
        })
    }
}

module.exports = TaskService
