const TABLE_NAME = 'task'

module.exports = {
    up: (queryInterface) =>
        queryInterface.bulkInsert(TABLE_NAME, [
            {
                id: 1,
                name: 'Compras',
                done: false,
            },
            {
                id: 2,
                name: 'Gym',
                done: true,
            },
        ]),
    down: (queryInterface) => queryInterface.bulkDelete(TABLE_NAME, null, {}),
}
