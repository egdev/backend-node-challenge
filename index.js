const express = require('express')
const app = express()
const config = require('./config')
const bodyParser = require('body-parser')
const TaskController = require('./controllers/task.controller')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Authorization, Content-Type, Accept')
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate')
    next()
})

app.get('/get-all-tasks-batch', TaskController.getBatch)
app.post('/update-task', TaskController.update)

app.listen(config.PORT, function () {
    console.log(`Example app listening on port ${config.PORT}!`)
})
