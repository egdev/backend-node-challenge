const TaskService = require('../services/task.service')

class TaskController {
    constructor() {
        this.taskService = new TaskService()
    }
    static getBatch = async (req, res) => {
        await this.taskService.getBatch(req.query.ids.split(','))
        res.send({
            message: 'Success',
            data: [],
        })
    }

    static delete = async (req, res) => {
        const task = await this.taskService.delete()
        res.send({
            message: 'Success',
            data: task,
        })
    }

    static update = async (req, res) => {
        const task = await this.taskService.update()
        res.send({
            message: 'Success',
            data: task,
        })
    }
}

module.exports = TaskController
