# Test Backend DigitalHouse

**Descripción**: Servicio de Tasks.

**Qué se determina con este test?**: la capacidad de resolver posibles problemas de performance y construcción de API Rest.

**Pasos para completar el test**:
1. Ir resolviendo los pequeños problemas que se van presentando en el desarrollo.
2. Revisar y corregir el path de los endpoints siguiendo conceptos de API Rest.
3. Mejorar el controller y service, buscando la posibilidad de hacer unit test haciendo inyección de dependencias.
4. En el service, revisar cada uno de los métodos y corregir los posibles problemas de performance encontrados.
5. Elegir un método de un servicio y escribir un test unitario.
6. Escribir un task.service.ts, adaptando el código y agregando los types e interfaces necesarios.
7. Escribir un Dockerfile capaz de levantar la api usando npm start.

**30 mins max.**

## Levantar el proyecto

```ssh
$ npm install
$ npm run sequelize db:migrate
$ npm run sequelize db:seed:all
$ npm start
```
